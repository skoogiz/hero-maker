const MONSTER_FORM = "#create-monster-form";

$(document).ready(() => {
    createForm();
    fetchMonsters();

    $("#add-ability").click((event) => {
        event.preventDefault();
        $("#abilities").append(createAbility());
    });

    $(MONSTER_FORM).submit(function(event) {
        event.preventDefault();
        const form = $(this);
        const req = {
            url: form.attr( "action" ),
            payload: {
                race: form.find( ":input[name='race']" ).val(),
                description: form.find( ":input[name='description']" ).val(),
                abilities: getAbilitiesArray()
            }
        };
        console.log("Create monster request", req);
        $.ajax({
            type: "POST",
            url: req.url,
            data: JSON.stringify(req.payload),
            contentType: "application/json",
            dataType: 'json',
            statusCode: {
                201: function() {
                    showSnackbar(`Nytt monster med namn "${req.payload.race}" skapat`);
                    resetForm();
                    fetchMonsters();
                }
            }
        });
    });

    
    $("#race-selector").change((event) => {
        event.preventDefault();
        const selected = $("#race-selector").find("option:selected");
        const data = selected.data('monster'); 
        $("#result-container").html(renderMonsterPanel(data));
        $("#sample-container").empty();
    });
});

const element = (tagName) => document.createElement(tagName);

const getAbilitiesArray = () => {
    let abilities = {};
    $(".ability").each(function() {
        const ability = $(this);
        abilities = {
            ...abilities,
            [ability.find( ":input[name='ability']" ).val()]: ability.find( ":input[name='dices']" ).val()
        };
    });
    return abilities;
};

const createAbility = () => {
    var ability = $(element("div"));
    $(ability).addClass("ability");
    $(ability).append(createInput({
        name: "ability",
        placeholder: "Grundegenskap",
        required: true
    }));
    $(ability).append(createInput({
        name: "dices", 
        placeholder: "Tärningsvärde",
        pattern: "^\\d+(D|T){1}\\d+?([\\+\\-\\*\\/]{1}\\d+)?$",
        required: true
    }));
    return ability;
};

const createRaceInput = () => {
    var input = $(element("input"));
    $(input)
        .attr("type", "text")
        .attr("placeholder", "Ras")
        .attr("name", "race")
        .attr("required", true);
    $("#race").append(input);
};

const createDescriptionInput = () => {
    var input = $(element("textarea"));
    $(input)
        .attr("placeholder", "Beskrivning")
        .attr("name", "description")
    $("#description").append(input);
};

const createInput = ({name, placeholder, pattern, required = false}) => {
    var textfield = $(element("div"));
    $(textfield).addClass("mui-textfield");
    var input = $(element("input"));
    $(input)
        .attr("type", "text")
        .attr("placeholder", placeholder)
        .attr("name", name)
        .attr("required", required);
    if (pattern) $(input).attr("pattern", pattern);
    $(textfield).append(input);
    return textfield;
};

const addSnackbar = (parent) => {
    var snackbar = $(element("div"));
    $(snackbar).addClass("snackbar");
    var text = $(element("div"));
    $(text).addClass("snackbar-text");
    $(snackbar).append(text);
    $(parent).append(snackbar);
};

const showSnackbar = (text) => {
    if (!($("#snackbar-root").length)) return;
    var root = $("#snackbar-root");
    if (!($(".snackbar").length)) addSnackbar(root);
    $(root).removeClass("snackbar-root-hidden");
    $(root).addClass("snackbar-root-visible");
    showSnackbarText(text);
    setTimeout(() => {
        hideSnackbarText();
        $(root).removeClass("snackbar-root-visible");
        $(root).addClass("snackbar-root-hidden");
    }, 3000);
};

const hideSnackbarText = () => {
    var snack = $(".snackbar-text");
    $(snack).removeClass("snackbar-text-visible");
    $(snack).addClass("snackbar-text-hidden");
    $(snack).html("");
};

const showSnackbarText = (text) => {
    var snack = $(".snackbar-text");
    $(snack).removeClass("snackbar-text-hidden");
    $(snack).addClass("snackbar-text-visible");
    var span = $(element("span"));
    $(span).text(text);
    $(snack).html(span);
};

const createForm = () => {
    createRaceInput();
    createDescriptionInput();
    $("#abilities").append(createAbility());
};

const resetForm = () => {
    $("#race").empty();
    $("#description").empty();
    $("#abilities").empty();
    createForm();
};

/* Populate monster select */
const renderOptions = (data) => {
    let sel = $("#race-selector");
    sel.empty();
    $.each(data, function(index, value) {
        var option = $(element("option"));
        $(option)
            .val(index)
            .text(value.race)
            .data("monster", value);
        sel.append(option);
    });
};

const renderMonsterPanel = (data) => {
    var panel = $(element("div"));
    $(panel).addClass("mui-panel");
    var p = $(element("p"));
    var heading = $(element("div"));
    $(heading).addClass("mui--text-title").text(data.race);
    var body = $(element("div"));
    $(body).addClass("mui--text-body1").text(data.description);
    var table = $(element("table"));
    $(table).addClass("mui-table").addClass("mui-table--bordered");
    var tableHead = $(element("thead"));
    var tableHeadRow = $(element("tr"));
    var tableHeadCellKey = $(element("th"));
    $(tableHeadCellKey).text("Grundegenskaper");
    var tableHeadCellVal = $(element("th"));
    $(tableHeadCellVal)
        .css("text-align", "center")
        .css("width", "40%")
        .text("Typvärde");
    $(tableHeadRow).append(tableHeadCellKey).append(tableHeadCellVal);
    $(tableHead).append(tableHeadRow);
    var tableBody = $(element("tbody"));
    $.each(data.abilities, function(key, value) {
        var tableBodyRow = $(element("tr"));
        $(tableBodyRow).attr("id", `attr_${key}`);
        var tableBodyCellKey = $(element("td"));
        $(tableBodyCellKey).css("display", "flex");
        $(tableBodyCellKey).css("flex-direction", "row");
        var abilityKey = $(element("span"));
        $(abilityKey).css("width", "50%");
        $(abilityKey).text(value.key);
        var abilityValue = $(element("span"));
        $(abilityValue).text(value.formula);
        $(tableBodyCellKey).append(abilityKey);
        $(tableBodyCellKey).append(abilityValue);
        var tableBodyCellVal = $(element("td"));
        $(tableBodyCellVal).attr("align", "center");
        $(tableBodyCellVal).text(value.mode);
        $(tableBodyRow).append(tableBodyCellKey).append(tableBodyCellVal);
        $(tableBody).append(tableBodyRow);
    });
    $(table).append(tableHead).append(tableBody);
    $(p).append(heading).append(body).append(table);
    $(panel).append(p);
    let button = $(element("button"));
    const buttonText = $(element("span"))
        .addClass("button-text-shadow")
        .text("Generera");
    $(button)
        .attr("id", "generate-monster")
        .attr("type", "submit")
        .addClass("large")
        .addClass("blue")
        .addClass("button")
        .addClass("full")
        .append(buttonText);
    $(button).bind("click", generateMonster(data));
    $(panel).append(button);
    return panel;
};

const renderValuePanel = (data) => {
    var panel = $(element("div"));
    $(panel).addClass("mui-panel");
    var table = $(element("table"));
    $(table).addClass("mui-table").addClass("mui-table--bordered");
    var tableHead = $(element("thead"));
    var tableHeadRow = $(element("tr"));
    var tableHeadCellKey = $(element("th"));
    $(tableHeadCellKey).text("Grundegenskaper");
    var tableHeadCellVal = $(element("th"));
    $(tableHeadCellVal)
        .css("text-align", "center")
        .css("width", "40%")
        .text("Värde");
    $(tableHeadRow).append(tableHeadCellKey).append(tableHeadCellVal);
    $(tableHead).append(tableHeadRow);
    var tableBody = $(element("tbody"));
    const row = (key, value) => {
        let tableBodyRow = $(element("tr"));
        let tableBodyCellKey = $(element("td"));
        $(tableBodyCellKey).text(key);
        let tableBodyCellVal = $(element("td"));
        $(tableBodyCellVal)
            .attr("align", "center")
            .text(value);
        $(tableBodyRow).append(tableBodyCellKey).append(tableBodyCellVal);
        return tableBodyRow;
    }
    $.each(data.abilities, function(key, value) {
        $(tableBody).append(row(key, value));
    });
    $(tableBody).append(row("KP", data.kp));
    $(tableBody).append(row("SB", data.sb || "-"));
    $(tableBody).append(row("Förflyttning", `L${data.movement}`));
    $(table).append(tableHead).append(tableBody);
    $(panel).append(table);
    return panel;
};

const generateMonster = (monster) => {
    return (event) => {
        event.preventDefault();
        $.get(`/api/v1/monsters/${monster.id}/personas`, function(data) {
            console.log("Generate monsters", data);
            $("#sample-container").html(renderValuePanel(data));
        }, "json" );
    };
};

const fetchMonsters = () => {
    $.get("/api/v1/monsters", function(data) {
        console.log("Get monsters", data);
        renderOptions(data.monsters || []);
    }, "json" );
};