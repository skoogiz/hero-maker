package com.skoogiz.heromaker.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Monster implements Serializable {
    private final Long id;
    private final String race;
    private final String description;
    private final List<Ability> abilities;

    private Monster(Long id, String race, String description, List<Ability> abilities) {
        this.id = id;
        this.race = race;
        this.description = description;
        this.abilities = abilities;
    }

    public Long getId() {
        return id;
    }

    public String getRace() {
        return race;
    }

    public String getDescription() {
        return description;
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public static MonsterBuilder builder() {
        return new Monster.MonsterBuilder();
    }

    public static class MonsterBuilder {
        private Long id;
        private String race;
        private String description;
        private List<Ability> abilities = new ArrayList<>();

        public MonsterBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public MonsterBuilder race(String race) {
            this.race = race;
            return this;
        }

        public MonsterBuilder description(String description) {
            this.description = description;
            return this;
        }

        public MonsterBuilder abilities(List<Ability> abilities) {
            this.abilities = abilities;
            return this;
        }

        public Monster build() {
            return new Monster(id, race, description, abilities);
        }
    }
}
