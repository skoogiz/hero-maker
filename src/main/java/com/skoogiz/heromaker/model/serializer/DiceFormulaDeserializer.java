package com.skoogiz.heromaker.model.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.skoogiz.heromaker.model.DiceFormula;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class DiceFormulaDeserializer extends JsonDeserializer<DiceFormula> {
    @Override
    public DiceFormula deserialize(JsonParser parser, DeserializationContext ctx) throws IOException {
        String formula = parser.getText();
        return DiceFormula.parse(formula);
    }
}
