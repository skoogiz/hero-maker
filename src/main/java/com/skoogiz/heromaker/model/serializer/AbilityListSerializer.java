package com.skoogiz.heromaker.model.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.skoogiz.heromaker.model.Ability;
import com.skoogiz.heromaker.model.DiceFormula;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AbilityListSerializer extends JsonSerializer<List<Ability>> {
    @Override
    public void serialize(List<Ability> value, JsonGenerator gen,
                          SerializerProvider serializers) throws IOException {
        Map<String, DiceFormula> map = new LinkedHashMap<>();
        for (Ability ability: value) {
            map.put(ability.getKey(), ability.getFormula());
        }
        gen.writeObject(map);
    }
}
