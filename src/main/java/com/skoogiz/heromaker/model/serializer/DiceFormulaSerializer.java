package com.skoogiz.heromaker.model.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.skoogiz.heromaker.model.DiceFormula;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class DiceFormulaSerializer extends JsonSerializer<DiceFormula> {
    @Override
    public void serialize(DiceFormula diceFormula, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeObject(diceFormula.toString());
    }
}
