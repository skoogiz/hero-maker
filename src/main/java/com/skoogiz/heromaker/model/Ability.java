package com.skoogiz.heromaker.model;

import java.io.Serializable;


public class Ability implements Serializable {
    private final Long id;
    private final String key;
    private final DiceFormula formula;

    private Ability(Long id, String key, DiceFormula formula) {
        this.id = id;
        this.key = key;
        this.formula = formula;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public DiceFormula getFormula() {
        return formula;
    }

    public Integer getMode() {
        return formula != null ? formula.mode() : null;
    }

    public static AbilityBuilder builder() {
        return new AbilityBuilder();
    }

    public static class AbilityBuilder {
        private Long id;
        private String key;
        private DiceFormula formula;

        public AbilityBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public AbilityBuilder key(String key) {
            this.key = key;
            return this;
        }

        public AbilityBuilder formula(DiceFormula formula) {
            this.formula = formula;
            return this;
        }

        public AbilityBuilder formula(String formula) {
            return formula(DiceFormula.parse(formula));
        }

        public Ability build() {
            return new Ability(id, key, formula);
        }
    }
}
