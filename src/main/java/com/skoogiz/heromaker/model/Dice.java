package com.skoogiz.heromaker.model;

import java.io.Serializable;
import java.util.Random;
import java.util.function.IntFunction;

/**
 * @author skoogiz
 *
 */
public class Dice implements Serializable {
    private static final Random DICE_RANDOMIZER = new Random();
    private static final IntFunction<Integer> RANDOMIZE = nrOfSides -> DICE_RANDOMIZER.nextInt(nrOfSides) + 1;

    private final int sides;

    private int currentValue;

    public Dice(int sides) {
        this.sides = sides;
        roll();
    }

    public int roll() {
        this.currentValue = RANDOMIZE.apply(sides);
        return peek();
    }

    public int getSides() {
        return sides;
    }

    public int peek() {
        return currentValue;
    }

}
