package com.skoogiz.heromaker.model;

import java.io.Serializable;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DiceFormula implements Serializable {

    public static final String DICE_FORMULA_REGEXP = "^\\d+(D|T)\\d+?([\\+\\-\\*\\/]\\d+)?$";
    public static final String OPERATION_REGEXP = "[\\+\\-\\*\\/]";
    public static final String DICE_REGEXP = "(D|T)";
    public static final Pattern OPERATION_PATTERN = Pattern.compile(OPERATION_REGEXP);

    private final int numberOfDiceRolls;
    private final Dice dice;
    private DiceModifier modifier;

    private DiceFormula(int numberOfDiceRolls, Dice dice, DiceModifier modifier) {
        this.numberOfDiceRolls = numberOfDiceRolls;
        this.dice = dice;
        this.modifier = modifier;
    }

    public int getNumberOfDiceRolls() {
        return numberOfDiceRolls;
    }

    public Dice getDice() {
        return dice;
    }

    public Optional<DiceModifier> getModifier() {
        return Optional.ofNullable(modifier);
    }

    public int resolve() {
        int result = 0;
        for (int i = 0; i < numberOfDiceRolls; i++) {
            result += dice.roll();
        }
        return modify(result);
    }

    private int modify(final int value) {
        return getModifier().map(mod -> mod.resolve(value)).orElse(value);
    }

    /**
     * Calculate the mode of the format.
     *
     * @return the mode of the format
     */
    public int mode() {
        int mode = 0;
        int sides = dice.getSides();
        for (int i = 0; i < numberOfDiceRolls; i++) {
            mode += (i%2) == 0 ? (sides / 2) + 1: sides / 2;
        }
        return modify(mode);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
                .append(numberOfDiceRolls)
                .append("T")
                .append(dice.getSides());
        getModifier().ifPresent(mod ->
                sb.append(mod.getOperator().getSign()).append(mod.getValue())
        );
        return sb.toString();
    }

    public static DiceFormula with(int numberOfDiceRolls, Dice dice, DiceModifier modifier) {
        return new DiceFormula(numberOfDiceRolls, dice, modifier);
    }

    public static DiceFormula with(int numberOfDiceRolls, Dice dice) {
        return new DiceFormula(numberOfDiceRolls, dice, null);
    }

    public static DiceFormula parse(String formula) {
        if (formula.toUpperCase().matches(DICE_FORMULA_REGEXP)) {
            int nr;
            int sides;

            Matcher matcher = OPERATION_PATTERN.matcher(formula);
            boolean hasModifier = matcher.find();
            int operationIndex = hasModifier ? matcher.start() : formula.length();

            DiceModifier modifier = hasModifier ? DiceModifier.of(
                    DiceModifier.Operator.of(formula.charAt(operationIndex)),
                    Integer.parseInt(formula.substring(operationIndex + 1))
            ) : null;


            String[] values = formula.substring(0, operationIndex).split(DICE_REGEXP);
            nr = Integer.parseInt(values[0]);
            sides = Integer.parseInt(values[1]);

            return with(nr, new Dice(sides), modifier);
        } else {
            throw new IllegalArgumentException("Illegal formula '" + formula + "'!");
        }
    }
}
