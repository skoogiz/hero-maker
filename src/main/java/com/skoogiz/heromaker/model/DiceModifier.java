package com.skoogiz.heromaker.model;

import java.io.Serializable;

public final class DiceModifier implements Serializable {
    private final Operator operator;
    private final int value;

    private DiceModifier(Operator operator, int value) {
        this.operator = operator;
        this.value = value;
    }

    public Operator getOperator() {
        return operator;
    }

    public int getValue() {
        return value;
    }

    public int resolve(int value) {
        switch (getOperator()) {
            case MINUS:
                return value - getValue();
            case MULTIPLY:
                return value * getValue();
            case DEVIDE:
                return value / getValue();
            default:
                return value + getValue();
        }
    }

    public static DiceModifier of(Operator op, int value) {
        return new DiceModifier(op, value);
    }

    public enum Operator {
        PLUS('+'), MINUS('-'), MULTIPLY('*'), DEVIDE('/');

        private char sign;

        Operator(char sign) {
            this.sign = sign;
        }

        public char getSign() {
            return sign;
        }

        public static Operator of(char op) {
            switch (op) {
                case '-':
                    return MINUS;
                case '*':
                    return MULTIPLY;
                case '/':
                    return DEVIDE;
                default:
                    return PLUS;
            }
        }
    }
}
