package com.skoogiz.heromaker.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.IntFunction;

public final class BaseAbilities {

    /**
     * Function calculates a characters hit points (KP) given the formula in Drakar och Demoner (1991) rules (page 24).
     *
     * @param sto the monsters given size
     * @param fys the monsters given physice
     * @return a characters hit point (KP) value
     */
    public static final IntFunction<Integer> FN_HIT_PINTS = (int sum) -> (sum + 1) / 2;

    /**
     * Calculate a characters hit points (KP) given the formula in Drakar och Demoner (1991) rules (page 24).
     *
     * @param sto the monsters given size
     * @param fys the monsters given physice
     * @return a characters hit point (KP) value
     */
    public static int calcHealth(int sto, int fys) {
        return calc(FN_HIT_PINTS, sto, fys);
    }

    /**
     * Function calculates a characters damage bonus given the formula in Drakar och Demoner (1991) rules (page 25).
     */
    public static final IntFunction<String> FN_DAMAGE_BONUS = (int sum) -> {
        String bonus = "";
        Map<Integer, String> thresholds = new LinkedHashMap<>();
        thresholds.put(27, "1");
        thresholds.put(30, "1T2");
        thresholds.put(33, "1T4");
        thresholds.put(41, "1T6");
        thresholds.put(51, "1T10");
        thresholds.put(61, "2T6");
        thresholds.put(81, "3T6");
        thresholds.put(101, "4T6");
        thresholds.put(141, "5T6");
        thresholds.put(181, "6T6");
        for (Map.Entry<Integer, String> threshold : thresholds.entrySet()) {
            if (sum >= threshold.getKey()) {
                bonus = threshold.getValue();
            } else {
                break;
            }
        }
        return bonus;
    };

    /**
     * Calculate a characters damage bonus given the formula in Drakar och Demoner (1991) rules (page 25).
     *
     * @param sty the monsters given strength
     * @param sto the monsters given size
     * @return a characters damage bonus
     */
    public static String calcDamageBonus(int sty, int sto) {
        return calc(FN_DAMAGE_BONUS, sty, sto);
    }

    /**
     * Function calculates a characters hit movement given the formula in Drakar och Demoner (1991) rules (page 25).
     */
    public static final IntFunction<Integer> FN_MOVEMENT = (int sum) -> {
        int movement = 7;
        for (int i = 12; i <= sum; i = i + 9) {
            movement++;
        }
        return movement;
    };

    /**
     * Calculate a characters hit movement given the formula in Drakar och Demoner (1991) rules (page 25).
     *
     * @param sto the monsters given size
     * @param fys the monsters given physice
     * @param smi the monsters given stealth
     * @return a characters movement value
     */
    public static int calcMovement(int sto, int fys, int smi) {
        return calc(FN_MOVEMENT, sto, fys, smi);

    }

    private static <T> T calc(IntFunction<T> fn, int... abilityValue) {
        int sum = 0;
        for (int val : abilityValue) {
            sum = sum + val;
        }
        return fn.apply(sum);
    }
}
