package com.skoogiz.heromaker;

import com.skoogiz.heromaker.repository.MonsterHelper;
import com.skoogiz.heromaker.repository.MonsterRepository;
import com.skoogiz.heromaker.repository.model.MonsterEO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeroMakerApplication implements CommandLineRunner {

    @Autowired
    private MonsterRepository monsterRepository;

    public static void main(String[] args) {
        SpringApplication.run(HeroMakerApplication.class, args);
    }

    @Override
    public void run(String... args) {
        // If database is empty add some sample monsters
        if (monsterRepository.count() == 0) {
            monsterRepository.save(MonsterEO.of(MonsterHelper.createDwarf()));
            monsterRepository.save(MonsterEO.of(MonsterHelper.createElf()));
            monsterRepository.save(MonsterEO.of(MonsterHelper.createHuman()));
            monsterRepository.save(MonsterEO.of(MonsterHelper.createOrc()));
        }
    }
}
