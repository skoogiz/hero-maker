package com.skoogiz.heromaker.controller;

import com.skoogiz.heromaker.controller.model.CreateMonsterRequest;
import com.skoogiz.heromaker.controller.model.MonsterListResponse;
import com.skoogiz.heromaker.controller.model.MonsterResponse;
import com.skoogiz.heromaker.controller.model.PersonaResponse;
import com.skoogiz.heromaker.model.Monster;
import com.skoogiz.heromaker.service.MonsterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/monsters")
public class MonsterController {

    @Autowired
    private MonsterService monsterService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity<Void> create(
            @RequestBody CreateMonsterRequest createRequest,
            UriComponentsBuilder uriBuilder) {
        Monster monster = monsterService.create(createRequest.map());
        return ResponseEntity
                .created(uriBuilder.path("/api/v1/monsters/{id}").buildAndExpand(monster.getId()).toUri())
                .build();
    }


    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MonsterListResponse fetchAll() {
        return MonsterListResponse.of(monsterService.fetchAll());
    }

    @GetMapping(
            path = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MonsterResponse fetch(@PathVariable("id") long id) {
        return monsterService.fetch(id)
                .map(MonsterResponse::of)
                .orElseThrow(() ->
                    new ResponseStatusException(HttpStatus.NOT_FOUND, "Monster Not Found")
                );
    }

    @GetMapping(
            path = "/{id}/personas",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PersonaResponse generatePersona(@PathVariable("id") long id) {
        PersonaResponse response = new PersonaResponse();
        monsterService.fetch(id).ifPresent(monster ->
                monster.getAbilities().forEach(ability ->
                        response.withAbility(ability.getKey(), ability.getFormula().resolve())
                )
        );
        return response;
    }
}
