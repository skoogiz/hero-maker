package com.skoogiz.heromaker.controller.model;

import com.skoogiz.heromaker.model.Monster;

import java.io.Serializable;

public class MonsterResponse implements Serializable {
    private Monster monster;

    private MonsterResponse(Monster monster) {
        this.monster = monster;
    }

    public Monster getMonster() {
        return monster;
    }

    public static MonsterResponse of(Monster monster) {
        return new MonsterResponse(monster);
    }
}
