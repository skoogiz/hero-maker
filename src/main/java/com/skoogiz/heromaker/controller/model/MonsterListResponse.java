package com.skoogiz.heromaker.controller.model;

import com.skoogiz.heromaker.model.Monster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MonsterListResponse implements Serializable {
    private List<Monster> monsters = new ArrayList<>();

    public List<Monster> getMonsters() {
        return monsters;
    }

    public static MonsterListResponse of(List<Monster> monsters) {
        MonsterListResponse response = new MonsterListResponse();
        response.monsters = monsters;
        return response;
    }
}
