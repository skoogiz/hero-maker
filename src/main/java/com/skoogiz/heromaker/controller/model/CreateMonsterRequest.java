package com.skoogiz.heromaker.controller.model;

import com.skoogiz.heromaker.model.Ability;
import com.skoogiz.heromaker.model.DiceFormula;
import com.skoogiz.heromaker.model.Monster;

import java.util.Map;
import java.util.stream.Collectors;

public class CreateMonsterRequest {
    private String race;
    private String description;
    private Map<String, DiceFormula> abilities;

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, DiceFormula> getAbilities() {
        return abilities;
    }

    public void setAbilities(Map<String, DiceFormula> abilities) {
        this.abilities = abilities;
    }

    /**
     * Map create request bean into an {@link Monster}.
     *
     * @return The new Monster
     */
    public Monster map() {
        return Monster.builder()
                .race(race)
                .description(description)
                .abilities(abilities.entrySet().stream()
                        .map(entry -> Ability.builder()
                                .key(entry.getKey())
                                .formula(entry.getValue())
                                .build())
                        .collect(Collectors.toList())
                )
                .build();
    }
}
