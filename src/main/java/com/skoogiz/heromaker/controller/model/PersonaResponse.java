package com.skoogiz.heromaker.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.skoogiz.heromaker.util.BaseAbilities;

import java.util.LinkedHashMap;
import java.util.Map;

public class PersonaResponse {
    private Map<String, Integer> abilities = new LinkedHashMap<>();

    public void withAbility(String key, Integer value) {
        this.abilities.put(key.toUpperCase(), value);
    }

    public Map<String, Integer> getAbilities() {
        return abilities;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)

    @JsonProperty("kp")
    public Integer getHealth() {
        if (!valuesExists("STO", "FYS")) {
            return null;
        }
        return BaseAbilities.calcHealth(value("STO"), value("FYS"));
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("sb")
    public String getDamageBonus() {
        if (!valuesExists("STY", "STO")) {
            return null;
        }
        return BaseAbilities.calcDamageBonus(value("STY"), value("STO"));
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("movement")
    public Integer getMovement() {
        if (!valuesExists("STO", "FYS", "SMI")) {
            return null;
        }
        return BaseAbilities.calcMovement(value("STO"), value("FYS"), value("SMI"));
    }

    private boolean valuesExists(String... attributeKeys) {
        boolean exists = false;
        for (String key : attributeKeys) {
            exists = abilities.containsKey(key);
            if (!exists) break;
        }
        return exists;
    }

    private int value(String key) {
        return abilities.get(key);
    }
}
