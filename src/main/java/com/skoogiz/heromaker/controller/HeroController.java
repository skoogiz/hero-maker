package com.skoogiz.heromaker.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/heroes")
public class HeroController {

    @PostMapping
    public void create(@RequestBody String data) {
        System.out.println(data);
    }

}
