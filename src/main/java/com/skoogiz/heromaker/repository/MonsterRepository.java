package com.skoogiz.heromaker.repository;

import com.skoogiz.heromaker.repository.model.MonsterEO;
import org.springframework.data.repository.CrudRepository;

public interface MonsterRepository extends CrudRepository<MonsterEO, Long> {
}
