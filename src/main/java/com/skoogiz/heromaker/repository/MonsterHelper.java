package com.skoogiz.heromaker.repository;

import com.skoogiz.heromaker.model.Ability;
import com.skoogiz.heromaker.model.Dice;
import com.skoogiz.heromaker.model.DiceFormula;
import com.skoogiz.heromaker.model.DiceModifier;
import com.skoogiz.heromaker.model.Monster;

import java.util.Arrays;

public class MonsterHelper {

    public static final DiceFormula DEFAULT_FORMULA = DiceFormula.with(3, new Dice(6));

    public static Monster createDwarf() {
        return Monster.builder()
                .race("Dvärg")
                .description(
                        "Dvärgar är inom fantasygenren en humanoid, småväxt och satt art (alltså inte en småvuxen människa) som är baserad på och har många likheter med de dvärgar som skildras i bland annat fornnordisk mytologi.")
                .abilities(Arrays.asList(
                        ability(AbilityType.STY, 4, 6),
                        ability(AbilityType.STO, 2, 6),
                        ability(AbilityType.FYS, 2, 6),
                        ability(AbilityType.SMI),
                        ability(AbilityType.INT),
                        ability(AbilityType.PSY),
                        ability(AbilityType.KAR)))
                .build();
    }

    public static Monster createElf() {
        return Monster.builder()
                .race("Alv")
                .description(
                        "Alver är ett folkslag som återkommer i många rollspel. Främsta anledningen till dess popularitet är att de har en framträdande roll i J.R.R. Tolkiens böcker och mytologi.")
                .abilities(Arrays.asList(
                        ability(AbilityType.STY, 2, 6),
                        ability(AbilityType.STO, 2, 6),
                        ability(AbilityType.FYS),
                        ability(AbilityType.SMI, 4, 6),
                        ability(AbilityType.INT, 4, 6),
                        ability(AbilityType.PSY),
                        ability(AbilityType.KAR)))
                .build();
    }

    public static Monster createHuman() {
        return Monster.builder()
                .race("Människa")
                .description(
                        "Människa (Homo sapiens = den visa/tänkande människan) är ett däggdjur av släktet Homo. Människan tillhör familjen hominider och som i sin tur tillhör ordningen primater. Alla nu levande människor tillhör underarten Homo sapiens sapiens.[1] Ytterligare en förmodad underart av människan är känd, den utdöda Homo sapiens idaltu. Till släktet homo ingår även de utdöda arterna neandertalmänniskan, Homo floresiensis och Homo erectus.")
                .abilities(Arrays.asList(
                        ability(AbilityType.STY),
                        ability(AbilityType.STO),
                        ability(AbilityType.FYS),
                        ability(AbilityType.SMI),
                        ability(AbilityType.INT),
                        ability(AbilityType.PSY),
                        ability(AbilityType.KAR)))
                .build();
    }

    public static Monster createOrc() {
        return Monster.builder()
                .race("Orch")
                .description(
                        "Orcherna eller orkerna (på engelska orc, från ett ord som används för att beskriva odjuret Grendel i Beowulf, eller från den romerska guden Orcus, dödsguden) är fiktiva humanoida varelser som först uppträdde i J.R.R. Tolkiens Midgård, vilket är vår egen värld med ett sagolikt förflutet, där de är varelser underställda mörkrets furste Melkor (senare Morgoth) och hans efterträdare Sauron.")
                .abilities(Arrays.asList(
                        ability(AbilityType.STY, 4, 6, 2),
                        ability(AbilityType.STO),
                        ability(AbilityType.FYS),
                        ability(AbilityType.SMI),
                        ability(AbilityType.INT, 2, 6),
                        ability(AbilityType.PSY),
                        ability(AbilityType.KAR, 2, 6)))
                .build();
    }

    private static DiceFormula formula(int nrOfDices, int sides) {
        return DiceFormula.with(nrOfDices, new Dice(sides));
    }

    private static DiceFormula formula(int nrOfDices, int sides, int plusModifier) {
        return DiceFormula.with(nrOfDices, new Dice(sides), DiceModifier.of(DiceModifier.Operator.PLUS, plusModifier));
    }

    private static Ability ability(AbilityType type) {
        return Ability.builder()
                .key(type.name())
                .formula(DEFAULT_FORMULA)
                .build();
    }

    private static Ability ability(AbilityType type, int nrOfDices, int sides) {
        return Ability.builder()
                .key(type.name())
                .formula(formula(nrOfDices, sides))
                .build();
    }

    private static Ability ability(AbilityType type, int nrOfDices, int sides, int mod) {
        return Ability.builder()
                .key(type.name())
                .formula(formula(nrOfDices, sides, mod))
                .build();
    }

    public enum AbilityType {
        STY, STO, FYS, SMI, INT, PSY, KAR
    }

}
