package com.skoogiz.heromaker.repository.model;

import com.skoogiz.heromaker.model.Monster;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class MonsterEO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String race;

    @Column(length = 512)
    private String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "post_id")
    private List<AbilityEO> abilities;

    public MonsterEO() {
    }

    private MonsterEO(Monster monster) {
        this.id = monster.getId();
        this.race = monster.getRace();
        this.description = monster.getDescription();
        this.abilities = monster.getAbilities()
                .stream()
                .map(AbilityEO::of)
                .collect(Collectors.toList());
    }

    public static MonsterEO of(Monster monster) {
        return new MonsterEO(monster);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AbilityEO> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<AbilityEO> abilities) {
        this.abilities = abilities;
    }

    public Monster map() {
        return toBuilder().build();
    }

    public Monster.MonsterBuilder toBuilder() {
        return Monster.builder()
                .id(id)
                .race(race)
                .description(description)
                .abilities(abilities.stream()
                        .map(AbilityEO::map)
                        .collect(Collectors.toList())
                );
    }
}
