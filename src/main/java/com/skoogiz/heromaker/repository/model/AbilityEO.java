package com.skoogiz.heromaker.repository.model;

import com.skoogiz.heromaker.model.Ability;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class AbilityEO implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String key;

    @Column(nullable = false)
    private String folrmula;

    public AbilityEO() {
    }

    private AbilityEO(Ability ability) {
        this.id = ability.getId();
        this.key = ability.getKey();
        this.folrmula = ability.getFormula().toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFolrmula() {
        return folrmula;
    }

    public void setFolrmula(String folrmula) {
        this.folrmula = folrmula;
    }

    public static AbilityEO of(Ability ability) {
        return new AbilityEO(ability);
    }

    public Ability map() {
        return toBuilder().build();
    }

    public Ability.AbilityBuilder toBuilder() {
        return Ability.builder().id(id).key(key).formula(folrmula);
    }
}
