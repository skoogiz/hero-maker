package com.skoogiz.heromaker.service;

import com.skoogiz.heromaker.model.Monster;
import com.skoogiz.heromaker.repository.MonsterRepository;
import com.skoogiz.heromaker.repository.model.MonsterEO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class MonsterService {

    @Autowired
    private MonsterRepository monsterRepository;

    public Monster create(Monster monster) {
        return monsterRepository.save(MonsterEO.of(monster)).map();
    }

    public Optional<Monster> fetch(long id) {
        Monster monster = monsterRepository.findById(id).map(MonsterEO::map).orElse(null);
        return Optional.ofNullable(monster);
    }

    public List<Monster> fetchAll() {
        Iterable<MonsterEO> monsters = monsterRepository.findAll();
        return StreamSupport.stream(monsters.spliterator(), false)
                .map(eo -> eo.toBuilder().build())
                .collect(Collectors.toList());
    }
}
