package com.skoogiz.heromaker.util;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class BaseAbilitiesStepDefs {

    // Single results
    private int sum;
    private int hitPoints;
    private String damageBonus;


    // Range results
    private int[] sums;
    private String[] damageBonusValues;
    private int[] movementValues;

    @Given("^a sum of \\(.+\\) that equals (\\d+)$")
    public void a_sum_of_abilities_that_equals(Integer sum) {
        this.sum = sum;
    }

    @Given("sums of \\(.+\\) that ranges between (\\d+) and (\\d+)$")
    public void sums_of_abilities_that_ranges_between(Integer min, Integer max) {
        this.sums = new int[max - min + 1];
        int currentSum = min;
        for(int i = 0; i < sums.length; i++) {
            sums[i] = currentSum++;
        }
    }

    @Given("^a sum of \\(.*\\) greater than (\\d+)$")
    public void a_sum_of_abilities_greater_than(Integer sum) {
        this.sum = sum + 1;
    }

    @When("^calculating hit points$")
    public void calculating_hit_points() {
        this.hitPoints = BaseAbilities.FN_HIT_PINTS.apply(sum);
    }

    @When("^calculating damage bonus$")
    public void calculating_damage_bonus() {
       this.damageBonus = BaseAbilities.FN_DAMAGE_BONUS.apply(sum);
    }

    @When("^calculating damage bonus values$")
    public void calculating_damage_bonus_values() {
        this.damageBonusValues = new String[sums.length];
        for(int i = 0; i < sums.length; i++) {
            damageBonusValues[i] = BaseAbilities.FN_DAMAGE_BONUS.apply(sums[i]);
        }
    }

    @When("^calculating movement values$")
    public void calculating_movement_values() {
        this.movementValues = new int[sums.length];
        for(int i = 0; i < sums.length; i++) {
            movementValues[i] = BaseAbilities.FN_MOVEMENT.apply(sums[i]);
        }
    }

    @Then("^the character have (\\d+) hit points$")
    public void the_character_have_hit_points(Integer hitPoints) {
        assertThat(this.hitPoints, is(hitPoints));
    }

    @Then("^should not have damage bonus values$")
    public void should_not_have_damage_bonus_values() {
        for(int i = 0; i < sums.length; i++) {
            assertThat(damageBonusValues[i], is(blankString()));
        }
    }

    @Then("^damage bonus should be (.*)$")
    public void damage_bonus_should_be(String bonus) {
        assertThat(damageBonus, is(bonus));
    }

    @Then("^damage bonus values should be (.*)$")
    public void damage_bonus_values_should_be(String bonus) {
        for(int i = 0; i < sums.length; i++) {
            assertThat(damageBonusValues[i], is(bonus));
        }
    }

    @Then("^movement values should be (\\d+)$")
    public void movement_values_should_be(Integer movement) {
        for(int i = 0; i < sums.length; i++) {
            assertThat(movementValues[i], is(movement));
        }
    }
}