Feature: Base abilities

  Scenario: calculate characters hit points (KP)
    Given a sum of (FYS + STO) that equals 22
    When calculating hit points
    Then the character have 11 hit points

  Scenario: calculate characters hit points (KP) rounded up
    Given a sum of (FYS + STO) that equals 23
    When calculating hit points
    Then the character have 12 hit points

  Scenario: calculate character abilities without damage bonus
    Given sums of (STY + STO) that ranges between 1 and 26
    When calculating damage bonus values
    Then should not have damage bonus values

  Scenario Outline: calculate damage bonus
    Given sums of (STY + STO) that ranges between <FROM> and <TO>
    When calculating damage bonus values
    Then damage bonus values should be <SB>

    Examples:
      | FROM | TO  | SB   |
      | 27   | 29  | 1    |
      | 30   | 32  | 1T2  |
      | 33   | 40  | 1T4  |
      | 41   | 50  | 1T6  |
      | 51   | 60  | 1T10 |
      | 61   | 80  | 2T6  |
      | 81   | 100 | 3T6  |
      | 101  | 140 | 4T6  |
      | 141  | 180 | 5T6  |

  Scenario: calculate character abilities with max damage bonus
    Given a sum of (STY + STO) greater than 180
    When calculating damage bonus
    Then damage bonus should be 6T6

  Scenario Outline: calculate movement
    Given sums of (STO + FYS + SMI) that ranges between <FROM> and <TO>
    When calculating movement values
    Then movement values should be <MV>

    Examples:
      | FROM | TO  | MV |
      | 0    | 11  | 7  |
      | 12   | 20  | 8  |
      | 21   | 29  | 9  |
      | 30   | 38  | 10 |
      | 39   | 47  | 11 |
      | 48   | 56  | 12 |
      | 57   | 65  | 13 |
      | 66   | 74  | 14 |
      | 75   | 83  | 15 |
      | 84   | 92  | 16 |
      | 93   | 101 | 17 |
      | 102  | 110 | 18 |
      | 111  | 119 | 19 |
      | 120  | 128 | 20 |
