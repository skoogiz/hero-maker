import React from "react";
import {render} from "react-dom";
import {Provider} from "react-redux";
import {ConnectedRouter} from "connected-react-router";
import * as serviceWorker from "./utils/serviceWorker";
import store, {history} from "./redux/store";
import App from "./containers/App";
import {GlobalStyle} from "./index.styles";

const target = document.querySelector("#root");

render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <GlobalStyle />
            <App />
        </ConnectedRouter>
    </Provider>,
    target
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
