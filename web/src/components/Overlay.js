// @flow
import React from "react";
import {Link} from "react-router-dom";
import {Container, CloseButton, Nav, List, ListItem} from "./Overlay.styles";

type Props = {
    open: boolean,
    onToggle: () => void
};

const Overlay = ({open, onToggle}: Props) => (
    <Container open={open}>
        <CloseButton type="button" onClick={onToggle}>
            Close
        </CloseButton>
        <Nav>
            <List>
                <ListItem>
                    <Link to="/monsters" onClick={onToggle}>
                        Monster
                    </Link>
                </ListItem>
                <ListItem>
                    <Link to="/about-us" onClick={onToggle}>
                        Om oss
                    </Link>
                </ListItem>
            </List>
        </Nav>
    </Container>
);

export default Overlay;
