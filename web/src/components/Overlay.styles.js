// @flow
import styled, {css} from "styled-components";
import cross from "../images/cross.png";

const open = css`
    opacity: 1;
    visibility: visible;
    transition: opacity 0.5s;
`;

const closed = css`
    opacity: 0;
    visibility: hidden;
    transition: opacity 0.5s, visibility 0s 0.5s;
`;

export const Container = styled.div`
    position: fixed;
    z-index: 1500;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: rgba(44, 52, 55, 0.9);
    background: linear-gradient(
        180deg,
        rgba(44, 52, 55, 0.9) 0%,
        rgba(56, 92, 64, 0.9) 35%,
        rgba(74, 153, 87, 0.9) 100%
    );
    ${props => (props.open ? open : closed)};
`;

export const CloseButton = styled.button`
    width: 80px;
    height: 80px;
    position: absolute;
    right: 20px;
    top: 20px;
    overflow: hidden;
    border: none;
    background: url(${cross}) no-repeat center center;
    text-indent: 200%;
    color: transparent;
    outline: none;
    z-index: 1600;
    cursor: pointer;
    opacity: 0.4;
    transition: transform 0.5s, opacity 0.5s;
    :hover,
    :focus {
        opacity: 1;
    }
`;

export const Nav = styled.nav`
    text-align: center;
    position: relative;
    top: 50%;
    height: 60%;
    font-size: 54px;
    transform: translateY(-50%);
    perspective: 1200px;

    @media screen and (max-height: 30.5em) {
        height: 70%;
        font-size: 34px;
    }
`;

const listOpen = css`
    opacity: 1;
    transform: rotateX(0deg);
`;

const listClosed = css`
    transform: translateY(25%) rotateX(-35deg);
`;

export const List = styled.ul`
    list-style: none;
    padding: 0;
    margin: 0 auto;
    display: inline-block;
    height: 100%;
    position: relative;
    opacity: 0.4;
    transform: translateY(-25%) rotateX(35deg);
    transition: transform 0.5s, opacity 0.5s;
    ${props => (props.open ? listOpen : listClosed)};
`;

export const ListItem = styled.li`
    display: block;
    height: 20%;
    height: calc(100% / 5);
    min-height: 54px;

    & > a {
        text-decoration: none;
        outline: none;
        font-weight: 300;
        display: block;
        color: #fff;
        transition: color 0.2s;
        :hover,
        :focus {
            color: #e3fcb1;
        }
    }

    @media screen and (max-height: 30.5em) {
        min-height: 34px;
    }
`;
