// @flow
import axios from "axios";
import * as MonsterActions from "../actions/monsterActions";

export const fetchMonster = (monsterId: number) => {
    return (dispatch: any) => {
        dispatch(MonsterActions.fetchMonster(monsterId));

        axios
            .get(`http://localhost:8008/api/v1/monsters/${monsterId}`)
            .then(res => {
                dispatch(MonsterActions.fetchMonsterSuccess(res.data.monster));
            })
            .catch(err => {
                dispatch(MonsterActions.fetchMonsterFailure(err.message));
            });
    };
};

export const fetchMonsters = () => {
    return (dispatch: any) => {
        dispatch(MonsterActions.fetchMonsters());

        axios
            .get(`http://localhost:8008/api/v1/monsters`)
            .then(res => {
                dispatch(MonsterActions.fetchMonstersSuccess(res.data.monsters));
            })
            .catch(err => {
                dispatch(MonsterActions.fetchMonstersFailure(err.message));
            });
    };
};
