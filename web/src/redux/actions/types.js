// @flow

export const FETCH_MONSTER_REQUEST: "monsters/FETCH_MONSTER_REQUEST" =
    "monsters/FETCH_MONSTER_REQUEST";
export const FETCH_MONSTER_RESPONSE: "monsters/FETCH_MONSTER_RESPONSE" =
    "monsters/FETCH_MONSTER_RESPONSE";

export const FETCH_MONSTERS_REQUEST: "monsters/FETCH_MONSTERS_REQUEST" =
    "monsters/FETCH_MONSTERS_REQUEST";
export const FETCH_MONSTERS_RESPONSE: "monsters/FETCH_MONSTERS_RESPONSE" =
    "monsters/FETCH_MONSTERS_RESPONSE";
