// @flow
import {type Monster} from "../../model/monster";
import {
    FETCH_MONSTER_REQUEST,
    FETCH_MONSTER_RESPONSE,
    FETCH_MONSTERS_REQUEST,
    FETCH_MONSTERS_RESPONSE
} from "./types";

type FetchMonsterAction = {
    type: typeof FETCH_MONSTER_REQUEST,
    payload: {
        monsterId: number
    }
};

type FetchMonsterSuccessAction = {
    type: typeof FETCH_MONSTER_RESPONSE,
    error: false,
    payload: {
        monster: Monster
    }
};

type FetchMonsterFailureAction = {
    type: typeof FETCH_MONSTER_RESPONSE,
    error: true,
    payload: {
        message: string
    }
};

type FetchMonstersAction = {
    type: typeof FETCH_MONSTERS_REQUEST
};

type FetchMonstersSuccessAction = {
    type: typeof FETCH_MONSTERS_RESPONSE,
    error: false,
    payload: {
        monsters: Monster[]
    }
};

type FetchMonstersFailureAction = {
    type: typeof FETCH_MONSTERS_RESPONSE,
    error: true,
    payload: {
        message: string
    }
};

export type MonsterAction =
    | FetchMonsterAction
    | FetchMonsterSuccessAction
    | FetchMonsterFailureAction
    | FetchMonstersAction
    | FetchMonstersSuccessAction
    | FetchMonstersFailureAction;

export const fetchMonster = (monsterId: number): FetchMonsterAction => ({
    type: FETCH_MONSTER_REQUEST,
    payload: {monsterId}
});

export const fetchMonsterSuccess = (monster: Monster): FetchMonsterSuccessAction => ({
    type: FETCH_MONSTER_RESPONSE,
    error: false,
    payload: {monster}
});

export const fetchMonsterFailure = (message: string): FetchMonsterFailureAction => ({
    type: FETCH_MONSTER_RESPONSE,
    error: true,
    payload: {message}
});

export const fetchMonsters = (): FetchMonstersAction => ({type: FETCH_MONSTERS_REQUEST});

export const fetchMonstersSuccess = (
    monsters: Monster[]
): FetchMonstersSuccessAction => ({
    type: FETCH_MONSTERS_RESPONSE,
    error: false,
    payload: {monsters}
});

export const fetchMonstersFailure = (message: string): FetchMonstersFailureAction => ({
    type: FETCH_MONSTERS_RESPONSE,
    error: true,
    payload: {message}
});
