// @flow

const activeMonster = state => state.monster.active;
const listMonsters = state => state.monster.monsters;

export const getMonster = state => activeMonster(state).monster;

export const getMonsters = state => listMonsters(state).monsters;
