import {combineReducers} from "redux";
import {connectRouter} from "connected-react-router";
import monster from "./monsterReducer";

const rootReducer = history =>
    combineReducers({
        router: connectRouter(history),
        monster
    });

export default rootReducer;
