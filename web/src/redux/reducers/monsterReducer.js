// @flow
import {combineReducers} from "redux";
import {type Monster} from "../../model/monster";
import type {MonsterAction} from "../actions/monsterActions";
import {
    FETCH_MONSTER_REQUEST,
    FETCH_MONSTER_RESPONSE,
    FETCH_MONSTERS_REQUEST,
    FETCH_MONSTERS_RESPONSE
} from "../actions/types";

type MonsterState = {
    loading: boolean,
    monster: ?Monster,
    error: ?{
        message: string
    }
};

type MonsterListState = {
    loading: boolean,
    monsters: Monster[],
    error: ?{
        message: string
    }
};

const initialState = {
    loading: false,
    monsters: null,
    error: null
};

const initialListState = {
    loading: false,
    monsters: [],
    error: null
};

const active = (
    state: MonsterState = initialState,
    action: MonsterAction
): MonsterState => {
    switch (action.type) {
        case FETCH_MONSTER_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_MONSTER_RESPONSE:
            if (action.error) {
                return {
                    ...state,
                    loading: false,
                    error: action.payload
                };
            }
            return {
                ...state,
                loading: false,
                error: null,
                monster: action.payload.monster
            };
        default:
            return state;
    }
};

const monsters = (
    state: MonsterListState = initialListState,
    action: MonsterAction
): MonsterListState => {
    switch (action.type) {
        case FETCH_MONSTERS_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_MONSTERS_RESPONSE:
            if (action.error) {
                return {
                    ...state,
                    loading: false,
                    error: action.payload
                };
            }
            return {
                ...state,
                loading: false,
                error: null,
                monsters: [...action.payload.monsters]
            };
        default:
            return state;
    }
};

const monstersReducer = combineReducers({
    active,
    monsters
});

export default monstersReducer;
