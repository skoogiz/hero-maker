import React from "react";
import {Route, Switch} from "react-router-dom";
import CreateMonsterPage from "../containers/pages/CreateMonsterPage";
import ListMonstersPage from "../containers/pages/ListMonstersPage";
import MonsterPage from "../containers/pages/MonsterPage";

const MonsterRoutes = () => (
    <Switch>
        <Route path="/monsters" exact component={ListMonstersPage} />
        <Route path="/monsters/create" exact component={CreateMonsterPage} />
        <Route path="/monsters/:monsterId" component={MonsterPage} />
    </Switch>
);

export default MonsterRoutes;
