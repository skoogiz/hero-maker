// @flow
import styled, {css} from "styled-components";

const header = {height: "56px"};

const mainLayout = css`
    width: 100%;
    padding: 0 2.5%;
`;

const mainContainer = css`
    width: 100%;
    max-width: 1024px;
    margin: 0 auto;
`;

export const Header = styled.header`
    background-color: #253a2b;
    color: #fffff0;
    ${mainLayout};
`;

export const HeaderContainer = styled.div`
    ${mainContainer};
    height: ${header.height};
    overflow: hidden;
    position: relative;
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    justify-content: space-between;
`;

export const MenuButton = styled.button`
    background-color: transparent;
    border: none;
    text-decoration: none;
    cursor: pointer;
    opacity: 0.8;

    :hover,
    :focus {
        opacity: 1;
    }
`;

export const Main = styled.main`
    ${mainLayout};
    min-height: calc(100vh - ${header.height});
`;

export const MainContainer = styled.div`
    ${mainContainer};
`;

export const Footer = styled.footer`
    background-color: #253a2b;
    ${mainLayout};
    min-height: 112px;
`;

export const FooterContainer = styled.div`
    ${mainContainer};
`;

export const Overlay = styled.div`
    height: ${props => (props.open ? "100vh" : 0)};
    width: ${props => (props.open ? "100vw" : 0)};
    position: fixed;
    z-index: 1500;
    top: 0;
    right: 0;
    background: rgba(44, 52, 55, 0.9);
    background: linear-gradient(
        180deg,
        rgba(44, 52, 55, 0.9) 0%,
        rgba(56, 92, 64, 0.9) 35%,
        rgba(74, 153, 87, 0.9) 100%
    );
    overflow-x: hidden;
    transition: 0.5s;
`;

export const OverlayCloseButton = styled.a`
    position: absolute;
    top: 4px;
    right: 6px;
    font-size: 100px;
    color: #818181;
    transition: 0.3s;
    :hover,
    :focus {
        color: #f1f1f1;
    }
`;

export const OverlayContent = styled.div`
    position: relative;
    top: 25%;
    width: 100%;
    text-align: center;
    margin-top: 30px;
    & > a {
        padding: 8px;
        text-decoration: none;
        font-size: 36px;
        color: #818181;
        display: block;
        transition: 0.3s;
        :hover,
        :focus {
            color: #f1f1f1;
        }
    }
`;
