// @flow
import styled, {css} from "styled-components";
import {Link} from "react-router-dom";

export const Container = styled.div`
    padding: 16px;
    margin: 0;
    with: 768px;
    min-height: calc(100vh - 56px);
    background-color: #121212;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
`;

export const Card = styled.div`
    padding: 0;
    margin: 0 0 12px;
    width: 100%;
    box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.14), 0 3px 3px -2px rgba(0, 0, 0, 0.12),
        0 1px 8px 0 rgba(0, 0, 0, 0.2);
`;

export const CardContent = styled.div`
    padding: 0;
    margin: 0;
    with: 100%;
    height: 56px;
    background-color: rgba(255, 255, 255, 0.11);
    border-radius: 4px 4px 0 0;
    color: rgba(255, 255, 255, 0.87);
`;

export const ColorBar = styled.div`
    padding: 0;
    margin: 0;
    witdh: 100%;
    height: 8px;
    background-color: #cac5c0;
    border-radius: 0 0 4px 4px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    flex-wrap: nowrap;
    overflow: hidden;
`;

export const ColorIdentity = styled.div`
    width: ${({size}) => `${size}%`};
    height: 100%;
    background-color: ${({color}) => color};
`;

/*
W: #F8F6D8
U: #C1D7E9
B: #9E9B99 | #BAB1AB |
R: #E49977
G: #A3C095
C: #CAC5C0
Bg: #0D0F0F
Gold: #B5AF72 | #D7CD9A | #B3A86D Gradient (edited)
*/
