// @flow
import React from "react";
import {Container, Card, CardContent, ColorBar, ColorIdentity} from "./About.styles";

const Item = () => (
    <Card>
        <CardContent>Test</CardContent>
        <ColorBar>
            <ColorIdentity color={"#F8F6D8"} size={33} />
            <ColorIdentity color={"#E49977"} size={33} />
            <ColorIdentity color={"#A3C095"} size={67} />
        </ColorBar>
    </Card>
);

export default () => (
    <Container>
        <Item></Item>
        <Card>
            <CardContent>Test</CardContent>
            <ColorBar>
                <ColorIdentity color={"#C1D7E9"} size={45} />
                <ColorIdentity color={"#9E9B99"} size={55} />
            </ColorBar>
        </Card>
        <Item></Item>
        <Item></Item>
    </Container>
);

/*
<h1>About Us</h1>
<p>Hello Medium!</p>
*/
