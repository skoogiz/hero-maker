// @flow
import React from "react";
import logo from "../images/logo.svg";
import * as styles from "./Learn.module.css";

const Learn = () => (
    <div>
        <img src={logo} className={styles.appLogo} alt="logo" />
        <p>
            Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
            className={styles.appLink}
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
        >
            Learn React
        </a>
    </div>
);

export default Learn;
