// @flow
import React from "react";
import {connect} from "react-redux";
import * as MonsterThunks from "../../redux/thunks/monsterThunks";
import type {Monster} from "../../model/monster";
import {getMonsters} from "../../redux/reducers/monsterSelectors";
import {
    Cards,
    Card,
    CardLink,
    CardCaption,
    Title,
    Description,
    TableContainer,
    Table,
    TableHead,
    TableCell
} from "./ListMonstersPage.styles";

type Props = {
    monsters: Monster[],
    fetchMonsters: () => void
};

class ListMonstersPage extends React.Component<Props> {
    componentDidMount() {
        const {fetchMonsters} = this.props;
        fetchMonsters();
    }

    render() {
        const {monsters} = this.props;
        return (
            <Cards>
                {monsters &&
                    monsters.map(monster => (
                        <CardLink
                            to={`/monsters/${monster.id}`}
                            key={`monsters_${monster.id}`}
                        >
                            <Card>
                                <CardCaption>
                                    <Title>{monster.race}</Title>
                                    <Description>{monster.description}</Description>
                                </CardCaption>
                                <TableContainer>
                                    <Table>
                                        {monster.abilities.map(ability => (
                                            <TableCell key={`ability_${ability.id}`}>
                                                <TableHead>{ability.key}</TableHead>
                                                {ability.formula}
                                            </TableCell>
                                        ))}
                                    </Table>
                                </TableContainer>
                            </Card>
                        </CardLink>
                    ))}
            </Cards>
        );
    }
}

const mapStateToProps = state => ({
    monsters: getMonsters(state)
});

const mapDispatchToProps = dispatch => ({
    fetchMonsters: () => dispatch(MonsterThunks.fetchMonsters())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ListMonstersPage);
