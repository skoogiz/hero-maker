// @flow
import React from "react";
import type {Match} from "react-router";
import {push} from "connected-react-router";
import {connect} from "react-redux";
import type {Monster} from "../../model/monster";
import {getMonster} from "../../redux/reducers/monsterSelectors";
import * as MonsterThunks from "../../redux/thunks/monsterThunks";
import {
    Card,
    CardCaption,
    Description,
    TableContainer,
    Title
} from "./ListMonstersPage.styles";

type Props = {
    match: Match,
    monster: Monster,
    fetchMonster: (monsterId: number) => void,
    gotoListMonstersPage: () => void
};

class MonsterPage extends React.Component<Props> {
    componentDidMount() {
        const {
            match: {params},
            fetchMonster
        } = this.props;
        fetchMonster(params.monsterId);
    }

    render() {
        const {monster, gotoListMonstersPage} = this.props;

        return (
            <div>
                {monster && (
                    <Card>
                        <CardCaption>
                            <Title>{monster.race}</Title>
                            <Description>{monster.description}</Description>
                        </CardCaption>
                        <TableContainer>
                            <table style={{margin: "1em auto", width: "80%"}}>
                                <tbody>
                                    <tr>
                                        <th colSpan={2}>Grundegenskap</th>
                                        <th>Typvärde</th>
                                    </tr>
                                    {monster.abilities.map(ability => (
                                        <tr key={`ability_${ability.id}`}>
                                            <th>{ability.key}</th>
                                            <td>{ability.formula}</td>
                                            <td>{ability.mode}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </TableContainer>
                    </Card>
                )}
                <button onClick={() => gotoListMonstersPage()}>
                    Utforska alla monster
                </button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    monster: getMonster(state)
});

const mapDispatchToProps = dispatch => ({
    gotoListMonstersPage: () => dispatch(push("/monsters")),
    fetchMonster: (monsterId: number) => dispatch(MonsterThunks.fetchMonster(monsterId))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MonsterPage);
