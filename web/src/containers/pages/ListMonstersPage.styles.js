// @flow
import styled, {css} from "styled-components";
import {Link} from "react-router-dom";

const green = css`
    background: linear-gradient(
        140deg,
        rgba(196, 218, 61, 1) 0%,
        rgba(110, 127, 14, 1) 69%,
        rgba(39, 80, 9, 1) 100%
    );
`;

const dod91 = css`
    background: linear-gradient(
        180deg,
        rgba(44, 52, 55, 0.8) 0%,
        rgba(56, 92, 64, 0.8) 35%,
        rgba(74, 153, 87, 0.8) 100%
    );
`;

export const CardLink = styled(Link)`
    text-decoration: none;
    color: inherit;

    :link {
        text-decoration: none;
        color: inherit;
    }

    :visited {
        text-decoration: none;
        color: inherit;
    }
`;

export const Cards = styled.div`
    margin: 1.5em 0 3em;
    display: grid;
    grid-template-columns: repeat(1, 1fr);
    column-gap: 1em;
    row-gap: 1em;

    @media (min-width: 1024px) {
        grid-template-columns: repeat(2, 1fr);
    }
`;

export const Card = styled.figure`
    padding: 1em;
    border-radius: 15px;
    margin: 4px;
    background: #ddd;
    text-align: left;
    display: flex;
    flex-direction: column;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
    transition: all 0.1s ease-in-out;

    h1,
    h2,
    h3,
    h4,
    h5 {
        margin: 0;
        font-weight: 400;
    }

    :hover {
        ${dod91};
    }

    :active {
        transform: scale(0.9, 0.9);
        box-shadow: 0 3px 12px rgba(0, 0, 0, 0.16), 0 3px 12px rgba(0, 0, 0, 0.23);
    }
`;

export const CardCaption = styled.figcaption`
    flex: 1 0;
    background-color: rgba(255, 255, 255, 0.65);
    padding: 1em;
    border-radius: 3px 3px 0 0;
    text-align: center;
`;

export const Title = styled.h1`
    text-align: center;
    font-size: 1.5em;
    font-weight: 500;
    letter-spacing: 0.02em;
`;

export const Description = styled.div`
    width: 100%;
    margin: 1em 0;
    overflow: hidden;
    font-size: 14px;
    line-height: 1.5;
    position: relative;
    height: 7.5em; /* exactly three lines */
`;

export const TableContainer = styled.div`
    color: #fff;
    background-color: rgba(0, 0, 0, 0.7);
    padding: 3px 3px 0;
    border-radius: 0 0 3px 3px;
`;

export const Table = styled.div`
    max-width: 512px;
    margin: 0 auto;
    display: grid;
    grid-template-columns: repeat(7, 1fr);
`;

export const TableHead = styled.span`
    font-size: 10px;
    text-transform: uppercase;
    font-weight: 400;
    display: block;
    margin-bottom: 3px;
`;

export const TableCell = styled.h4`
    color: #fff;
    padding: 1.5em 0;
    font-size: 14px;
    text-align: center;
`;
