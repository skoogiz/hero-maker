// @flow
import React from "react";
import {Link, Redirect, Route, Switch} from "react-router-dom";
import MonsterRoutes from "../routes/MonstersRoutes";
import About from "./About";
import Home from "./Home";
import {
    Header,
    HeaderContainer,
    MenuButton,
    Main,
    MainContainer,
    Footer,
    FooterContainer
} from "./App.styles";
import * as styles from "./App.module.css";
import Overlay from "../components/Overlay";
import ScrollIcon from "../components/ScrollIcon";

const App = () => {
    const [overlayOpen, setOverlayOpen] = React.useState(false);

    const toggleOverlay = () => setOverlayOpen(!overlayOpen);

    return (
        <>
            <Header role="nav">
                <HeaderContainer>
                    <Link to="/" className={styles.title}>
                        Hero Maker
                    </Link>
                    <MenuButton onClick={toggleOverlay}>
                        <ScrollIcon size={28} color={"#fffff9"} />
                    </MenuButton>
                </HeaderContainer>
            </Header>
            <Main>
                <MainContainer>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/about-us" component={About} />
                        <MonsterRoutes />
                        <Redirect to="/" />
                    </Switch>
                </MainContainer>
            </Main>
            <Footer>
                <FooterContainer />
            </Footer>
            <Overlay open={overlayOpen} onToggle={toggleOverlay} />
        </>
    );
};

export default App;
