// @flow
import React from "react";
import {push} from "connected-react-router";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

const Home = (props) => (
    <div>
        <h1>Hem</h1>
        <p>Välkommen!</p>
        <button onClick={() => props.changePage()}>Utforska alla monster</button>
    </div>
);

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            changePage: () => push("/monsters")
        },
        dispatch
    );

export default connect(
    null,
    mapDispatchToProps
)(Home);
