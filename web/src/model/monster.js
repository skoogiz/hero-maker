// @flow

export type Ability = {
    id: number,
    key: string,
    formula: string,
    mode: number
};

export type Monster = {
    id: number,
    race: string,
    description: string,
    abilities: Ability[]
};

