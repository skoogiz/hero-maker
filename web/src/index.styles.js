import {createGlobalStyle} from "styled-components";

export const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
    }
    
    *, *:before, *:after {
        box-sizing: border-box;
    }
    
    html, body { height: 100%; }
    
    body {
        margin: 0;
        padding: 0;
        background-color: #FFF;
        font-size: 130%;
        font-family: "Roboto", sans-serif;
        font-weight: 300;
        line-height: 1.55em;
        text-rendering: optimizeLegibility;
        color: #555;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
    
    h1, h2, h3, h4, h5, h6 {
        color: #333;
        font-weight: 800;
        text-rendering: optimizeLegibility;
    }
    
    h1 {
        font-size: 170%;
        margin-top: 1em;
        margin-bottom: 0.5em;
        line-height: 1.4em;
    }
    
    h2 {
        font-size: 150%;
        margin-top: 1em;
        margin-bottom: 0.5em;
        line-height: 1.4em;
    }
    
    h3 {
        font-size: 130%;
        margin-top: 3.25em;
        margin-bottom: 0.5em;
        line-height: 1.4em;
    }
    
    h4 {
        font-size: 110%;
        margin-top: 2em;
        margin-bottom: 0.5em;
        line-height: 1.4em;
    }
    
    h5 {
        font-size: 100%;
        margin-top: 1.5em;
        margin-bottom: 0.5em;
        line-height: 1.4em;
    }
    
    p {
        margin: 0.5em 0 2.5em;
    }
    
    blockquote {
        padding: 0.1em 1.3em;
        background: #fafafa;
        border-left: 5px #81E26D solid;
        color: #666;
    }
    
    pre {
        font-family: 'PT Mono', Consolas, "Inconsolata", "Courier New", monospace;
        overflow: auto;
        margin: 1em auto;
    }
    
    code {
        font-family: 'PT Mono', Consolas, "Inconsolata", "Courier New", monospace;
    }
    
    @media screen and (max-width: 1000px) {
        body {
            font-size: 130%;
        }
    
        pre code {
            line-height: 1.6em;
        }
    }
    
    @media screen and (max-width: 800px) {
        body {
            font-size: 125%;
        }
    
        pre code {
            font-size: 80%;
        }
    }
    
    @media screen and (max-width: 500px) {
        body {
            font-size: 120%;
        }
    }
    
    @media screen and (max-width: 320px) {
        body {
            padding: 0;
        }
    }
`;
