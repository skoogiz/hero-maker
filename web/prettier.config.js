module.exports = {
    parser: "babel",
    trailingComma: "none",
    useTabs: false,
    tabWidth: 4,
    jsxBracketSameLine: false,
    semi: true,
    bracketSpacing: false,
    printWidth: 90,
    overrides: [
        {
            files: "package.json",
            options: {
                tabWidth: 2,
                parser: "json"
            }
        },
        {
            files: "*.ts",
            options: {
                parser: "typescript"
            }
        }
    ]
};
